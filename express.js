var express = require('express');
var bodyParser = require('body-parser');
var https = require('https');
var http = require('http');
var fs = require('fs');
var url = require('url');
var app = express();
var basicAuth = require('basic-auth-connect');

app.use(bodyParser());

var auth = basicAuth(function(user, pass) {
    return((user ==='cs360')&&(pass === 'test'));
  });

var options = {
  host: '127.0.0.1',
  key: fs.readFileSync('ssl/server.key'),
  cert: fs.readFileSync('ssl/server.crt')
};
http.createServer(app).listen(80);
https.createServer(options, app).listen(443);
app.use('/', express.static('./html', {maxAge: 60*60*1000}));
app.get('/getcity', function (req, res) {

    var urlObj = url.parse(req.url, true, false);

  console.log("URL path "+urlObj.pathname);
  console.log("URL search "+urlObj.search);
  console.log("URL query "+urlObj.query["q"]);

  if(urlObj.pathname.indexOf("getcity") !=-1) {
   // Execute the REST service 
   console.log("In REST Service");

   var myRe = new RegExp("^"+urlObj.query["q"]);
   console.log(myRe);

   fs.readFile('cities.dat.txt', function (err, data) {
    if(err) throw err;
    var cities = data.toString().split("\n");
    var jsonresult = [];

    for(var i = 0; i < cities.length; i++) {
      var result = cities[i].search(myRe);
      if(result != -1) {
        console.log(cities[i]);
        jsonresult.push({city:cities[i]});
      }
    }

    console.log(JSON.stringify(jsonresult));
    res.writeHead(200);
    res.end(JSON.stringify(jsonresult));
   });
} 
});

  app.get('/comments', function (req, res) {
    console.log("In GET");
    
      var MongoClient = require('mongodb').MongoClient;
      MongoClient.connect("mongodb://localhost/weather", function(err, db) {
        if(err) throw err;

        
        db.collection("comments", function(err, comments){
          if(err) throw err;
        
        
          comments.find(function(err, items){
            items.toArray(function(err, itemArr){
              console.log("Document Array: ");
              console.log(itemArr);
              res.json(itemArr);
              //res.writeHead(200);
              res.end("");
              //res.end(JSON.stringify(itemArr));
            });
          });
        });
      });
  });
  app.post('/comment', auth, function (req, res) {
    console.log("POST comment route");
    console.log("In POST comment route");
    console.log(req.user);
    console.log("Remote User");
    console.log(req.remoteUser);
    console.log(req.body);

        var reqObj = req.body;

        var MongoClient = require('mongodb').MongoClient;
        MongoClient.connect("mongodb://localhost/weather", function(err, db) {
          if(err) throw err;
          db.collection('comments').insert(reqObj,function(err, records) {
            //console.log("Record added as "+records[0]._id);
          });
        });
        res.writeHead(200);
        res.end("");
    
  });



